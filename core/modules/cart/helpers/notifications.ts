import { router } from '@vue-storefront/core/app';
import { currentStoreView, localizedRoute } from '@vue-storefront/core/lib/multistore';
import i18n from '@vue-storefront/i18n';
import config from 'config';

const proceedToCheckoutAction = () => ({
  label: i18n.t('Proceed to checkout'),
  action: () => router.push(localizedRoute('/checkout', currentStoreView().storeCode))
});
const checkoutAction = () => !config.externalCheckout ? proceedToCheckoutAction() : null;

const productAddedToCart = () => ({
  type: 'success',
  message: '<svg data-v-83fdf1b2="" width="25" height="22" viewBox="0 0 25 22" fill="none" xmlns="http://www.w3.org/2000/svg" class="mr5"><g data-v-83fdf1b2="" id="Cart"><g data-v-83fdf1b2="" id="Rectangle"><path data-v-83fdf1b2="" id="Rectangle_2" d="M1.98742 7.13556C1.90673 6.39593 2.48603 5.75 3.23005 5.75H21.6309C22.5797 5.75 23.1826 6.7656 22.7283 7.59857L18.9101 14.5986C18.6911 15.0001 18.2702 15.25 17.8127 15.25H3.99368C3.35579 15.25 2.82023 14.7697 2.75106 14.1356L1.98742 7.13556Z" fill="#67B6C3" fill-opacity="1" stroke="#67B6C3" stroke-width="1.5"></path></g> <path data-v-83fdf1b2="" id="Path 3" d="M1 1.5H6" stroke="#67B6C3" stroke-width="1.5" stroke-linecap="round"></path> <circle data-v-83fdf1b2="" id="Oval" cx="5.5" cy="20.5" r="1.5" fill="#67B6C3"></circle> <circle data-v-83fdf1b2="" id="Oval Copy 6" cx="13.9" cy="20.5" r="1.5" fill="#67B6C3"></circle></g></svg><span class="cl-gray-666666">' + i18n.t('Product has been added to the cart!') + '</span>',
  action1: { label: i18n.t('OK') },
  action2: checkoutAction()
})

const productQuantityUpdated = () => ({
  type: 'success',
  message: '<svg data-v-83fdf1b2="" width="25" height="22" viewBox="0 0 25 22" fill="none" xmlns="http://www.w3.org/2000/svg" class="mr5"><g data-v-83fdf1b2="" id="Cart"><g data-v-83fdf1b2="" id="Rectangle"><path data-v-83fdf1b2="" id="Rectangle_2" d="M1.98742 7.13556C1.90673 6.39593 2.48603 5.75 3.23005 5.75H21.6309C22.5797 5.75 23.1826 6.7656 22.7283 7.59857L18.9101 14.5986C18.6911 15.0001 18.2702 15.25 17.8127 15.25H3.99368C3.35579 15.25 2.82023 14.7697 2.75106 14.1356L1.98742 7.13556Z" fill="#67B6C3" fill-opacity="1" stroke="#67B6C3" stroke-width="1.5"></path></g> <path data-v-83fdf1b2="" id="Path 3" d="M1 1.5H6" stroke="#67B6C3" stroke-width="1.5" stroke-linecap="round"></path> <circle data-v-83fdf1b2="" id="Oval" cx="5.5" cy="20.5" r="1.5" fill="#67B6C3"></circle> <circle data-v-83fdf1b2="" id="Oval Copy 6" cx="13.9" cy="20.5" r="1.5" fill="#67B6C3"></circle></g></svg><span class="cl-gray-666666">' + i18n.t('Product quantity has been updated!') + '</span>',
  action1: { label: i18n.t('OK') },
  action2: checkoutAction()
})

const unsafeQuantity = () => ({
  type: 'warning',
  message: i18n.t(
    'The system is not sure about the stock quantity (volatile). Product has been added to the cart for pre-reservation.'
  ),
  action1: { label: i18n.t('OK') }
})

const outOfStock = () => ({
  type: 'error',
  message: i18n.t('The product is out of stock and cannot be added to the cart!'),
  action1: { label: i18n.t('OK') }
})

const createNotification = ({ type, message }) => ({ type, message, action1: { label: i18n.t('OK') } })
const createNotifications = ({ type, messages }) =>
  messages.map(message => createNotification({ type, message }));

export {
  createNotification,
  createNotifications,
  productAddedToCart,
  productQuantityUpdated,
  unsafeQuantity,
  outOfStock
};
