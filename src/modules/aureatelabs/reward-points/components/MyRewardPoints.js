import { UserAccount } from '@vue-storefront/core/modules/user/components/UserAccount'

// Component deprecated, now in User module

export default {
  name: 'MyRewardPoints',
  mixins: [UserAccount]
}
