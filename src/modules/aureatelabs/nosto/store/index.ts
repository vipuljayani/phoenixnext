import { Module } from 'vuex'
import NostoState from '../types/NostoState'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'
import RootState from '@vue-storefront/core/types/RootState'
export const nostoModule: Module<NostoState, RootState> = {
  namespaced: true,
  state: {
    recommendations: [],
    nostolist: [],
    ids: []
  },
  getters,
  actions,
  mutations
}
