import Vue from 'vue'
import { ActionTree } from 'vuex';
import FreeGiftState from '../types/FreeGiftState';
import rootStore from '@vue-storefront/core/store'
import * as types from './mutation-types'
import { StorageManager } from '@vue-storefront/core/lib/storage-manager'

const actions: ActionTree<FreeGiftState, any> = {
  async list (context, quoteId) {
    let url = rootStore.state.config.api.url + rootStore.state.config.aureatelabs.aureate_free_gift;

    try {
      await fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ quote_id: quoteId })
      })
        .then(res => res.json())
        .then((resp) => {
          if (resp.code === 200) {
            context.commit(types.FREE_GIFT_FETCH, resp.result[0]);
          }
        });
    } catch (e) {
      console.log(e.message)
    }
  },

  async addItem (context, parameters) {
    let quote = parameters[0]
    let ruleId = parameters[1]
    let giftId = parameters[2]
    let url = rootStore.state.config.api.url + rootStore.state.config.aureatelabs.aureate_free_gift_add;

    try {
      await fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ quote: quote, rule: ruleId, gift: giftId })
      })
        .then(res => res.json())
        .then((resp) => {
          if (resp.code === 200) {
            console.log('Added the free gift product successfully!')
          }
        });
    } catch (e) {
      console.log(e.message)
    }
  }
}

export default actions
