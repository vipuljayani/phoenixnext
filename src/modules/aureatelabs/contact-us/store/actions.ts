import Vue from 'vue'
import { ActionTree } from 'vuex';
import ContactState from '../types/ContactState'
import rootStore from '@vue-storefront/core/store'
import Contact from '../types/Contact'
import i18n from '@vue-storefront/i18n'
import { adjustMultistoreApiUrl } from '@vue-storefront/core/lib/multistore'
import EventBus from '@vue-storefront/core/compatibility/plugins/event-bus'

const actions: ActionTree<ContactState, any> = {
  async add (context, contactData: Contact) {
    let url = rootStore.state.config.aureatelabs.contact.create_endpoint
    if (rootStore.state.config.storeViews.multistore) {
      url = adjustMultistoreApiUrl(url)
    }
    EventBus.$emit('notification-progress-start', i18n.t('Processing...'))
    try {
      await fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(contactData)
      })
      rootStore.dispatch('notification/spawnNotification', {
        type: 'success',
        message: i18n.t('Thanks for contacting us with your comments and questions. We\'ll respond to you very soon.'),
        action1: { label: i18n.t('OK') }
      })
      Vue.prototype.$bus.$emit('clear-add-contact-form')
      EventBus.$emit('notification-progress-stop')
    } catch (e) {
      rootStore.dispatch('notification/spawnNotification', {
        type: 'error',
        message: i18n.t('Something went wrong. Try again in a few seconds.'),
        action1: { label: i18n.t('OK') }
      })
      EventBus.$emit('notification-progress-stop')
    };
  }
}

export default actions
