import { Module } from 'vuex'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'
export const QuickLinksModuleStore: Module<any, any> = {
  namespaced: true,
  state: {
    quicklinks: []
  },
  mutations,
  actions,
  getters
}
