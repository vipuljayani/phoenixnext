<?php 
	require "utils/PaymentGatewayHelper.php";

	$secret_key = "D5327FACFC1AADF7DBCC7CF725367950B12807B54354E5A407C574C75492063E"; 

	$response = $_REQUEST["paymentResponse"];
	$reponsePayLoadXML = base64_decode($response);
	
	//Parse ResponseXML
	$xmlObject =simplexml_load_string($reponsePayLoadXML) or die("Error: Cannot create object");
	
	//Decode payload with base64 to get the Reponse
	$payloadxml = base64_decode($xmlObject->payload);
	
	//Get the signature from the ResponseXML
	$signaturexml = $xmlObject->signature;
	
	//Encode the payload
	$base64EncodedPayloadResponse=base64_encode($payloadxml);
	//Generate signature based on "payload"
	$signatureHash = strtoupper(hash_hmac('sha256', $base64EncodedPayloadResponse ,$secret_key, false));
	
	//Compare the response signature with payload signature with secretKey
	header('Content-Type: text/xml');
	if($signaturexml == $signatureHash){
		$xml = new SimpleXMLElement($payloadxml);
		echo $xml->asXML();
	}
	else{
		//If Signature does not match
		echo "Error :<br/><textarea style='width:100%;height:20px'>". "Wrong Signature"."</textarea>"; 
		echo "<br/>";
	}